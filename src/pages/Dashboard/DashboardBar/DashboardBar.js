import React from 'react';
import './DashboardBar.css';

const DashboardBar = () => {
    return (
<div className='wrapper'>       
    <div className='sidebar'>
        <ul>
            <li className='li-dashboard'>Produtos e categorias</li>
            <li className='li-dashboard'>Usuários</li>
            <li className='li-dashboard'>Estoque</li>
            <li className='li-dashboard'>Administração</li>
        </ul> 
    </div>
</div> 
    );
}
export default DashboardBar;