import React from 'react';
import './Footer.css';
import Facebook from '../../../image/facebook.png'
import Instagram from '../../../image/instagram.png'
import Twitter from '../../../image/twitter.png'

const Footer = () => {
    return (
        <footer>
            <div className='Footer1 Container' >
                <div>
                    <div id='Footer2'>
                        <a href='https://www.facebook.com/injunioruff'><img  src= { Facebook }/></a>
                        <a href='https://www.instagram.com/injunioruff'><img  src= { Instagram }/></a>
                        <a href='https://www.twitter.com/injunioruff'><img  src= { Twitter }/></a>
                    </div>
                    <p id='copyright'>© Copyright 2020 ShoppINg</p>
                </div>
                <div>
                    <p id='desenvolvido'>Desenvolvido por<br/>Grupo 3</p>
                </div>    
            </div>
        </footer>
    );
}
export default Footer