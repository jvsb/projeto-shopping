import React ,{useEffect, useState} from 'react';
import axios from 'axios';
import Sofa from '../../../image/Sofa.jpg'
import Sobrenos from '../../../image/sobrenos.png'
import './Principal1.css'
const Principal1 = (props) =>{
    const [product, setProduct] = useState(props.product)
    useEffect( () => {
        axios.get('https://projetofinalin.herokuapp.com/products/')
        .then((response)=>{
            setProduct(response.data.id);
        })
    }, [])
    return(
        <main>
            <section id="sct1pcpl">
                <picture>
                    <img id='img-intro' src={ Sofa } alt="Imagem chamativa de sofá" />
                </picture> 
                <h1 id="titulo1">e-Commerce ShoppINg</h1>
            </section>
            <section id="sct2pcpl">
                <h2 className="titulo2">Produtos mais vendidos!</h2>
                <div id="prod_pcpl">
                    <div className='prod_div'></div>
                    <div className='prod_div'></div>
                    <div className='prod_div'></div>
                </div>
            </section>
            <section id="sct3pcpl">
                <h2 className="titulo3">Sobre nós</h2>
                <div id="sobre_pcpl" className="Container">
                    <div className='prod_div'>
                        <img id='img-sobre' src={Sobrenos} alt="Membros da empresa" />
                    </div>
                    <div className='prod_div'>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lacinia diam fringilla mauris tristique sodales.
                        Aliquam nec dolor augue. Sed sollicitudin commodo massa, at tincidunt eros venenatis vitae. Mauris mollis vitae augue quis laoreet.
                        Maecenas tempor lacinia libero, eu dignissim dolor condimentum in. Vestibulum dapibus tortor quis libero sodales, nec posuere velit iaculis.
                        Donec varius lorem neque, eu vestibulum lectus blandit et. Duis porttitor suscipit magna non viverra. Maecenas at lorem nulla.
                        Donec ac tortor sed est auctor euismod non sed dui.
                        </p>
                    </div>
                </div>
            </section>
        </main>
    );
}
export default Principal1