import React ,{useState, useEffect} from 'react';
import {Link, Redirect} from 'react-router-dom';
import './HeaderUser.css';
import Logo from '../../../image/logo.png'
import AuthenticationServices from '../../Services/Auth';



const HeaderAdmin = (props) => {
    const [redirect, useRedirect] = useState(false);

       const Logout = () => {
        props.updateLogged(false);
        localStorage.removeItem('token');
        useRedirect(true);
       }
       if(redirect){
           return <Redirect to='/'/>
       }
  return (
    <header>
    <nav className="Container">
        <Link className= "logo" to="/"><img id="logoimg" src={ Logo } />ShoppINg</Link>
        <ul>
            <li>
                <Link id="fontmenu" to="/TodosProd">Produtos</Link>
            </li>
            <li>
                <Link id="fontmenu" to="/ProfileEdit">Perfil</Link>
            </li>
            <li>
                <Link id="fontmenu" to="/" onClick={Logout}>Sair</Link>
            </li>
            <li>
                <Link id="fontmenu" to="/Dashboard">Dashboard</Link>
            </li>
        </ul>
    </nav>
</header>
  )
}
export default HeaderAdmin;