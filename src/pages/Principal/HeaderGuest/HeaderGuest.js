import React from 'react';
import {Link} from 'react-router-dom';
import './HeaderGuest.css';
import Logo from '../../../image/logo.png'


function HeaderGuest () {

  return (
        <header>
            <nav className="Container">
                <Link className= "logo" to="/"><img id="logoimg" src={ Logo } />ShoppINg</Link>
                <ul>
                    <li>
                        <Link id="fontmenu" to="/TodosProd">Produtos</Link>
                    </li>
                    <li>
                        <Link id="fontmenu" to="/Login">Login</Link>
                    </li>
                    <li>
                        <Link id="fontmenu" to="/Register">Cadastro</Link>
                    </li>
                </ul>
            </nav>
        </header>
  );    
}
export default HeaderGuest;