import axios from 'axios';


const api = axios.create({
  baseURL: "https://projetofinalin.herokuapp.com",
});

async function loginOn(email, password) {
  const axiosConfig = {
    headers: {
      "Content-Type": "application/json",      
    },
  };
  try {
    const postyData = await api.post("/login",
    {
      "user":{
         "email":email,
         "password": password
          }
        }
    , axiosConfig);
    const res = postyData.data;
    if (res) {
      localStorage.setItem("user", JSON.stringify(res));
    }
    return res;
  } catch (error) {
    console.log("error")
  }
}
export default api
