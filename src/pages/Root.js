import React ,{useState, useEffect} from  'react';
import { BrowserRouter as Router, Switch, Route, Redirect} from 'react-router-dom';
import RegisterPage from './Form/Register/Registerpage'
import LoginPage from './Form/Login/Loginpage'
import Principal from './Principal/PrincipalGuest/PrincipalGuest'
import HeaderGuest from './Principal/HeaderGuest/HeaderGuest'
import HeaderAdmin from './Principal/HeaderUser/HeaderAdmin'
import HeaderUser from './Principal/HeaderUser/HeaderUser'
import AuthenticationServices from './Services/Auth';
import PrincipalGuest from './Principal/PrincipalGuest/PrincipalGuest';
import PrincipalUser from './Principal/PrincipalUser/PrincipalUser'
import TodosProd from './Produtos/TodosProd/TodosProd'
import Principal1 from './Principal/PrincipalGuest/Principal1'
import Footer from './Principal/Footer/Footer';
import Produto from  './Produtos/IndivProd/Produto'
import ProfileEdit from './Form/Profile/ProfileEdit';
import DashboardBar from './Dashboard/DashboardBar/DashboardBar';
import ProdutoSolo from './Produtos/IndivProd/ProdutoSolo'


const Root = () => {
    const [isLogged, setIsLogged] =  useState(false);
    const [admin, setAdmin] = useState(false);
    useEffect(() =>{
        AuthenticationServices(setIsLogged, setAdmin)
    }, [])
    console.log(setIsLogged)
    const updateLogged = (value) =>{
        setIsLogged(value);
    }
    const updateUser = (value) =>{
        setAdmin(value);
    }
    return(
        <div>
        <Router>
            {!isLogged? <HeaderGuest/>:
             admin?<HeaderAdmin updateLogged = {updateLogged} />:<HeaderUser updateLogged = {updateLogged} />}
            <Switch>
            <Route exact path="/">
            <Principal1/>
            </Route>
            <Route exact path="/Login">
            {isLogged? <Redirect to = "/"/>:<LoginPage updateLogged= {updateLogged} updateUser = {updateUser} />}
            </Route>
            <Route exact path="/Register">
            {isLogged? <Redirect to = "/"/>:<RegisterPage/>}
            </Route>
            <Route exact path="/TodosProd">
            <TodosProd updateLogged = { (value) =>{ setIsLogged(value) } } />
            </Route>
            <Route exact path="/produto/:productId" component={Produto}>
            </Route>
            <Route exact path="/ProfileEdit">
            {!isLogged? <Redirect to = "/"/>:<ProfileEdit/>}
            </Route>
            <Route exact path="/Dashboard">
                <DashboardBar/>
            </Route>
            <Route exact path="/Product/1">
                <ProdutoSolo/>
            </Route>
            {/* <Route exact path="/PrincipalUser">
            {!isLogged? <Redirect to = "/"/>:<PrincipalUser/>}
            </Route> */}
                {/* <Route path="/Principal" component={Principal}/>
                <Route path="/Login" component={Login}/>
                <Route path="/Register" component={Register}/>
                 <Route>{isLogged? <Redirect to = "/Principal"/>:<Login/>}</Route> */}
            </Switch>
            <Footer/>
        </Router>
        </div>
    );
}
export default Root;