import React, { useState }from 'react';
import { Redirect, Link } from 'react-router-dom';
import Interrogation from '../../../image/Point_d_interrogation.jpg'
import './Produto.css'

const Produto = (props) => {
    const [product, setProduct] = useState(props.product)
    //const [isLogged, setisLogged] = useState({});
    // props.updateLogged();

    const perfilProduct = (props) => {
        // <Redirect to='/Productprofile'/>
    }

    return (
        <div className="prod">
            <figure className="prod_solo">
                <img className="prod_img" src = {Interrogation} alt="Foto do produto"/>
                <figcaption>{product.name }</figcaption>   
            </figure>
            <p>Recebeu {product.likes} likes</p>
            <p>{product.category}</p>
            <p>{product.description}</p>
            <h5>R$ {product.price}</h5>
            <button>VER MAIS</button>
        </div>
    );
} 
export default Produto
