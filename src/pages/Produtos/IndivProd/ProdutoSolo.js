import React from 'react';
import './ProdutoSolo.css'

const ProdutoSolo = () => {
    const likeCont = () =>{
        const cont = 0;
        cont += 1;
    }
    return(
        <div className='div1'>
            <p className='p-description'>Sed pretium ligula dictum sem euismod, in rutrum turpis posuere. Sed ultricies metus vitae tortor ornare elementum. Nunc et purus sodales, posuere risus id, feugiat quam. Donec auctor congue elementum. Suspendisse nec ipsum neque. Fusce at vestibulum lectus. Praesent eget tellus pellentesque, volutpat mi id, malesuada nisl. Integer sit amet rhoncus mauris, eget convallis magna. Sed ut dui nibh. Quisque ut nulla vel ipsum vestibulum imperdiet quis vel purus. Fusce eleifend dui eget nulla malesuada, at malesuada quam porta. In et ante vitae erat varius facilisis.</p>
            <p className='p-1'>Avaliações do produto:</p>
            <div className='your-commentary'>
                <p className='txt'>Avalie esse produto:</p>
                <textarea type='textarea' className='textareaC' cols="30" rows="5"></textarea>
                <button type='submit' className='submitcomment'>Enviar</button>
            </div>
            <div className='commentary'>
                <p className='author-commentary'>Usuário 1</p>
                <p className='txt-commentary'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed in ante sit amet odio luctus suscipit. Nulla in dui mauris. Nulla vitae justo commodo nunc porta placerat tincidunt ut urna. Nunc non convallis tellus. Donec consequat mauris ac semper tempus.</p>
            </div>
            <div className='commentary'>
                <p className='author-commentary'>Usuário 2</p>
                <p className='txt-commentary'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed in ante sit amet odio luctus suscipit. Nulla in dui mauris. Nulla vitae justo commodo nunc porta placerat tincidunt ut urna. Nunc non convallis tellus. Donec consequat mauris ac semper tempus.</p>
            </div>
            <div className='commentary'>
                <p className='author-commentary'>Usuário 3</p>
                <p className='txt-commentary'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed in ante sit amet odio luctus suscipit. Nulla in dui mauris. Nulla vitae justo commodo nunc porta placerat tincidunt ut urna. Nunc non convallis tellus. Donec consequat mauris ac semper tempus.</p>
            </div>
            <div className='commentary'>
                <p className='author-commentary'>Usuário 4</p>
                <p className='txt-commentary'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed in ante sit amet odio luctus suscipit. Nulla in dui mauris. Nulla vitae justo commodo nunc porta placerat tincidunt ut urna. Nunc non convallis tellus. Donec consequat mauris ac semper tempus.</p>
            </div>
            <div className='commentary'>
                <p className='author-commentary'>Usuário 5</p>
                <p className='txt-commentary'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed in ante sit amet odio luctus suscipit. Nulla in dui mauris. Nulla vitae justo commodo nunc porta placerat tincidunt ut urna. Nunc non convallis tellus. Donec consequat mauris ac semper tempus.</p>
            </div>
        </div>
    );
}
export default ProdutoSolo