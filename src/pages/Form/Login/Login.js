import React, { useState } from 'react';
import api from '../../Services/Api';
import history from '../../../history';
import './Login.css';
import { Redirect } from 'react-router-dom';

const Login = (props) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [redirect, setRedirect] = useState(false);
    const handleLogin = async (e) => {
      e.preventDefault();
        let loginbody = {
                
                "email": email,
                "password": password
          
      }
     const response = await api.post('/login', loginbody)
     if(response.status === 200){
         props.updateLogged(true);
         props.updateUser(response.data.user.admin);
         console.log(response)
         setRedirect(true)
         localStorage.setItem('token',response.data.token)
     }
     else{
        console.log(response.status)
     }
    }

    const handleInputChange = e => {
        const value = e.target.value

        switch (e.target.name) {
            case "loginname":
                setEmail(value)
                break
            case "loginpassword":
                setPassword(value)
                break
            default:
        }
    }
        if(redirect){
            return <Redirect to='/'/>
        }
        return  (
          <div className="formulogin">
            <div>
                <form onSubmit={handleLogin} onChange={handleInputChange}>
                <p className='logintxt'>Fazer Login</p>
                <label>Nome</label>
                <input type="email" name="loginname"/>
                <label>Senha</label>
                <input type="password" name="loginpassword"/>
                
                <input type="submit" value="Entrar"/>
                </form>
            </div>
          </div>
        )  
    }
export default Login