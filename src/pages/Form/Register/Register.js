import React, { useState, useRef } from 'react';
import {Redirect} from 'react-router-dom';
import './Register.css';
import axios from 'axios';

const Register = (props) => { //Tentei com useRef mas estava dando Erro :( 
    
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [password2, setPassword2] = useState('');
    const [cep, setCep] = useState('');
    const [endereco, setEndereco] = useState('');
    const [numero, setNumero] = useState('');
    const [cidade, setCidade] = useState('');
    const [bairro, setBairro] = useState('');
    const [uf, setUF] = useState('');
    const [fixo, setFixo] = useState('');
    const [celular, setCelular] = useState('');
    const [redirect, setRedirect] = useState(false);
    

    
    const handleInputChange = e => {
        const value = e.target.value;

        if(e.target.name === "name"){
            setName(value);
        }
        else if(e.target.name === "email"){
            setEmail(value);
        }
        else if(e.target.name === "password"){
            setPassword(value);
        }
        else if(e.target.name === "cep"){
            setCep(value);
        }
        else if(e.target.name === "endereco"){
            setEndereco(value);
        }
        else if(e.target.name === "numero"){
            setNumero(value);
        }
        else if(e.target.name === "cidade"){
            setCidade(value);
        }
        else if(e.target.name === "bairro"){
            setBairro(value);
        }
        else if(e.target.name === "uf"){
            setUF(value);
        }
        else if(e.target.name === "fixo"){
            setFixo(value);
        }
        else if(e.target.name === "celular"){
            setCelular(value);
        }
        else{
            setPassword2(value);
        }
        
    }
     
    const handleInputCep = (e, setFieldValue) => {
        const { value } = e.target;
        const cep = value?.replace(/[^0-9]/g, '');
        if(cep?.length !== 8){
            return;
        }

        fetch(`https://viacep.com.br/ws/${value}/json`)
        .then((res) => res.json())
        .then((data) => console.log(data));

        //setFieldValue('endereco', data.endereco);
    }

    const handleSubmit = async e => {
        e.preventDefault()
        
        const postBody = {
            "user": {
                "name": name,
                "email": email,
                "password": password,
                "password_confirmation": password2,
                "cep": cep,
               // "admin": 0,
                "endereco": endereco,
                "numero": numero,
                "cidade": cidade,
                "bairro": bairro,
                "uf": uf,
                "fixo": fixo,
                "celular": celular
            }
        }
        console.log(postBody)
   const response = await axios.post('https://projetofinalin.herokuapp.com/sign_up', postBody)
      if(response.status === 200){
        setRedirect(true)
    }
    else{
       console.log(response.status)
    }
        if(redirect){
            return <Redirect to='/Login'/>
        }
        else{
            console.log("deu ruim");
        }
    }

    return (
        <div>
            <form className="formu" onSubmit={handleSubmit}>
                <p>Fazer Cadastro</p>
            <label>Seu nome:</label>
            <input type="text" name="name"placeholder="Ex: João" onChange={handleInputChange}/>
            <label>Seu email:</label>
            <input type="email" name="email" placeholder="Email@hotmail.com" onChange={handleInputChange}/>
            <label>Sua senha:</label>
            <input type="password" name="password" placeholder="Senha" onChange={handleInputChange}/>
            <label>Confirme sua senha:</label>
            <input type="password" name="password2" placeholder="Repita a senha" onChange={handleInputChange}/>
                <label>CEP:</label>
                <input type="text" name="cep" onChange={handleInputChange}/>
                <label>Endereço:</label>
                <input type="text" name="endereco" onChange={handleInputChange}/>
            <div>
                <label>Número:</label>
                <input type="text" name="numero"  onChange={handleInputChange}/>
                <label>Cidade:</label>
                <input type="text" name="cidade"  onChange={handleInputChange}/>
                <label>Bairro:</label>
                <input type="text" name="bairro" onChange={handleInputChange}/>
                <label>UF:</label>
                <input type="text" name="uf" onChange={handleInputChange}/>
            </div>
            <p className="telefone">Telefones</p>
            <label>Fixo:</label>
            <input type="text" name="fixo" onChange={handleInputChange}/>
            <label>Celular:</label>
            <input type="text" name="celular" onChange={handleInputChange}/>
            <input type="submit" value="Registrar"/>
            </form>
            </div>
    );

}
export default Register;